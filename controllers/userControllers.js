/* eslint-disable camelcase */
const bcrypt = require('bcryptjs');
const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');

// @desc Get user's profile info
// @route GET /api/users/me
// @access Private
const getUser = asyncHandler(async (req, res) => {
  const {_id, role, email, created_date} = await User.findById(req.user.id);
  res.status(200).json({
    user: {
      _id,
      role,
      email,
      created_date,
    },
  });
});

// @desc Delete user's profile
// @route DELETE /api/users/me
// @access Private
const deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user.id);

  if (!user) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await User.deleteOne({_id: req.user.id});

  res.status(200).json({
    message: 'Profile deleted successfully',
  });
});

// @desc Change user's password
// @route PATCH /api/users/me/password
// @access Private
const changePassword = asyncHandler(async (req, res) => {
  const oldPassword = req.body.oldPassword;
  const newPassword = req.body.newPassword;

  const user = await User.findById(req.user.id);

  if (await bcrypt.compare(oldPassword, user.password)) {
    // hash password

    const salt = await bcrypt.genSalt(8);
    const hashedPassword = await bcrypt.hash(newPassword, salt);

    await User.findByIdAndUpdate(
        user.id,
        {password: hashedPassword},
        {
          new: true,
        },
    );
    res.status(200).json({
      message: 'Password changed successfully',
    });
  } else {
    res.status(400);
    throw new Error('Please write correct password');
  }
});

module.exports = {getUser, deleteUser, changePassword};
