/* eslint-disable new-cap */
const express = require('express');
const {
  getLoads,
  addLoad,
  getActiveLoads,
  iterateToNextLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingById,
} = require('../controllers/loadControllers');

const protectRoute = require('../middlewares/authMiddleware');

const router = express.Router();

// Get user's loads
router.get('/', protectRoute, getLoads);
// Add Load for User
router.post('/', protectRoute, addLoad);
// Get user's active load(if exists)
router.get('/active', protectRoute, getActiveLoads);
// Iterate to next Load state
router.patch('/active/state', protectRoute, iterateToNextLoadState);
// Get user's Load by id
router.get('/:id', protectRoute, getLoadById);
// Update user's load by id
router.put('/:id', protectRoute, updateLoadById);
// Delete user's load by id
router.delete('/:id', protectRoute, deleteLoadById);
// Post a user's load by id
router.post('/:id/post', protectRoute, postLoadById);
// Get user's Load shipping info by id
router.get('/:id/shipping_info', protectRoute, getLoadShippingById);

module.exports = router;
