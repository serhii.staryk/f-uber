/* eslint-disable new-cap */
const express = require('express');
const {
  getUser,
  deleteUser,
  changePassword,
} = require('../controllers/userControllers');

const protectRoute = require('../middlewares/authMiddleware');

const router = express.Router();

// Get user's profile info
router.get('/', protectRoute, getUser);
// Delete user's profile
router.delete('/', protectRoute, deleteUser);
// Change user's password
router.patch('/password', protectRoute, changePassword);

module.exports = router;
