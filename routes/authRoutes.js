/* eslint-disable new-cap */
const express = require('express');
const {
  registerUser,
  loginUser,
  restorePassword,
} = require('../controllers/authControllers');

const router = express.Router();

// Register a new system user(Shipper or Driver)
router.post('/register', registerUser);
// Login
router.post('/login', loginUser);
// Forgot password
router.post('/forgot_password', restorePassword);

module.exports = router;
