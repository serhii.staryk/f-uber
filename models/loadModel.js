/* eslint-disable new-cap */
const mongoose = require('mongoose');

const loadSchema = mongoose.Schema(
    {
      created_by: {
        type: String,
      },
      assigned_to: {
        type: String,
      },
      status: {
        type: String,
      },
      state: {
        type: String,
      },
      name: {
        type: String,
        required: [true, 'Please add a name'],
      },
      payload: {
        type: Number,
        required: [true, 'Please add a payload'],
      },
      pickup_address: {
        type: String,
        required: [true, 'Please add a pickup_address'],
      },
      delivery_address: {
        type: String,
        required: [true, 'Please add a delivery_address'],
      },
      dimensions: {
        width: {type: Number, required: true, min: 0},
        length: {type: Number, required: true, min: 0},
        height: {type: Number, required: true, min: 0},
      },
      logs: {
        type: Array,
      },
      created_date: {
        type: String,
      },
    },
    {versionKey: false},
);

module.exports = mongoose.model('Load', loadSchema);
