const isExist = (arr, checkPhrase) => arr.includes(checkPhrase);

module.exports = isExist;
