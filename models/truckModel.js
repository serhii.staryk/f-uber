/* eslint-disable new-cap */
const mongoose = require('mongoose');

const truckSchema = mongoose.Schema(
    {
      created_by: {
        type: String,
      },
      assigned_to: {
        type: String,
      },
      type: {
        type: String,
        required: [true, 'Please add a type'],
      },
      status: {
        type: String,
      },
      created_date: {
        type: String,
      },
    },
    {versionKey: false},
);

module.exports = mongoose.model('Truck', truckSchema);
