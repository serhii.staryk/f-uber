const statusTruck = ['OL', 'IS'];

const typeTruck = [
  {
    name: 'SPRINTER',
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  {
    name: 'SMALL STRAIGHT',
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  {
    name: 'LARGE STRAIGHT',
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
];

module.exports.statusTruck = statusTruck;
module.exports.typeTruck = typeTruck;
