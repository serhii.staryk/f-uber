const moongoose = require("mongoose");

const connectToDB = async () => {
  try {
    const connToDb = await moongoose.connect(process.env.MONGO_URI);

    console.log(
      `MongoDB connected ${connToDb.connection.host}`.yellow.underline
    );
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

module.exports = connectToDB;
