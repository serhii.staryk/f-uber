const statusLoad = {
  NEW: 'En route to Pick Up',
  POSTED: 'Arrived to Pick Up',
  ASSIGNED: 'En route to delivery',
  SHIPPED: 'Arrived to delivery',
};

module.exports = statusLoad;
