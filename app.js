/* eslint-disable no-unused-vars */
const express = require('express');
const cors = require('cors');
const colors = require('colors');
const bodyParser = require('body-parser');

const dotenv = require('dotenv').config();
const errorHandler = require('./middlewares/errorHandlerMiddleware');

const authRoutes = require('./routes/authRoutes');
const loadRoutes = require('./routes/loadRoutes');
const truckRoutes = require('./routes/truckRouters');
const userRoutes = require('./routes/userRoutes');
const morgan = require('morgan');

const connectToDb = require('./config/db');

const port = process.env.PORT || 8080;

connectToDb();
const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cors());

app.use(
    morgan(':method :url :status :res[content-length] - :response-time ms'),
);

app.use('/api/auth', authRoutes);
app.use('/api/loads', loadRoutes);
app.use('/api/trucks', truckRoutes);
app.use('/api/users/me', userRoutes);

app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server runs on port ${port}`.blue.underline);
});
