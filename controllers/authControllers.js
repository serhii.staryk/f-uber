const asyncHandler = require('express-async-handler');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');

const User = require('../models/userModel');
const isExist = require('../utils/isExist');

const userRoles = require('../models/types/userType');

// @desc Register a new system user(Shipper or Driver)
// @route POST /api/auth/register
// @access Public
const registerUser = asyncHandler(async (req, res) => {
  const {email, password, role} = req.body;

  if (!email || !password || !role) {
    res.status(200);
    throw new Error('Please add all fields');
  }

  // check if user exist
  const userExist = await User.findOne({email});

  if (userExist) {
    res.status(400);
    throw new Error('Email already exists');
  }

  // check role
  if (!isExist(userRoles, role)) {
    res.status(400);
    throw new Error(
        `${role} is not allowed. Only ${userRoles.join(' or ')} are allowed`,
    );
  }

  // hash password
  const salt = await bcrypt.genSalt(8);
  const hashedPass = await bcrypt.hash(password, salt);

  // create user
  await User.create({
    email,
    password: hashedPass,
    role,
    created_date: new Date().toISOString(),
  });

  res.status(200).json({
    message: 'Profile created successfully',
  });
});

// @desc Login
// @route POST /api/auth/login
// @access Public
const loginUser = asyncHandler(async (req, res) => {
  const {email, password} = req.body;

  // check for user
  const user = await User.findOne({email});

  if (user && (await bcrypt.compare(password, user.password))) {
    res.status(200).json({
      jwt_token: generateToken(user.id),
    });
  } else {
    res.status(400);
    throw new Error('Invalid credentials');
  }
});

const generateToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {expiresIn: '30d'});
};

// @desc Forgot password
// @route POST /api/auth/forgot_password
// @access Public
const restorePassword = asyncHandler(async (req, res) => {
  const {email} = req.body;
  const newPass = Math.random().toString(36).slice(2, 10);
  // check for user
  const user = await User.findOne({email});

  if (user) {
    // hash password
    const salt = await bcrypt.genSalt(8);
    const hashedPass = await bcrypt.hash(newPass, salt);

    // change password
    await User.findByIdAndUpdate(
        user.id,
        {password: hashedPass},
        {
          new: true,
        },
    );
  } else {
    res.status(400);
    throw new Error('User is not existed');
  }

  const mailData = {
    from: process.env.GMAIL_LOGIN,
    to: email,
    subject: 'Home task 3',
    text: 'You forgot your password',
    html: `
            <h1>Hey there! </h1><br>
            <p> This is your new password ${newPass}<p/>`,
  };

  transporter.sendMail(mailData, (error, info) => {
    if (error) {
      return console.log(error);
    }
    res.status(200).json({message: 'New password sent to your email address'});
  });
});

const transporter = nodemailer.createTransport({
  port: 465,
  host: 'smtp.gmail.com',
  auth: {
    user: process.env.GMAIL_LOGIN,
    pass: process.env.GMAIL_PASSWORD,
  },
  secure: true,
});

module.exports = {registerUser, loginUser, restorePassword};
