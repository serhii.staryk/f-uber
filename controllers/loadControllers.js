/* eslint-disable max-len */
/* eslint-disable camelcase */
const asyncHandler = require('express-async-handler');

const Load = require('../models/loadModel');
const User = require('../models/userModel');
const Truck = require('../models/truckModel');

const loadTypes = require('../models/types/loadType');
const typeUser = require('../models/types/userType');
const {typeTruck, statusTruck} = require('../models/types/truckType');

const statusLoadKey = Object.keys(loadTypes);
const statusLoadValue = Object.values(loadTypes);

const checkRole = require('../utils/checkRole');

// @desc Get user's loads
// @route GET /api/loads
// @access Private
const getLoads = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user.id);
  const status = req.query.status;
  const offset = +req.query.offset || 0;
  const limit = +req.query.limit || 10;

  if (status) {
    if ((user.role === typeUser[0])) {
      const loads = await Load.find({
        created_by: user.id,
        status,
      })
          .skip(offset)
          .limit(limit);
      return res.status(200).json({
        loads,
      });
    }

    if ((user.role === typeUser[1])) {
      const loads = await Load.find({
        assigned_to: user.id,
        status,
      })
          .skip(offset)
          .limit(limit);
      return res.status(200).json({
        loads,
      });
    }
  }

  if ((user.role === typeUser[0])) {
    const loads = await Load.find({
      created_by: user.id,
    })
        .skip(offset)
        .limit(limit);
    return res.status(200).json({
      loads,
    });
  }

  if ((user.role === typeUser[1])) {
    const loads = await Load.find({
      assigned_to: user.id,
    })
        .skip(offset)
        .limit(limit);
    return res.status(200).json({
      loads,
    });
  }
});

// @desc Add Load for User
// @route POST /api/loads
// @access Private
const addLoad = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user.id);
  const {name, payload, pickup_address, delivery_address, dimensions} =
    req.body;
  const {width, length, height} = dimensions;

  checkRole(user.role, typeUser[0], res);

  await Load.create({
    created_by: user.id,
    assigned_to: null,
    status: statusLoadKey[0],
    state: statusLoadValue[0],
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions: {
      width,
      length,
      height,
    },
    logs: [],
    created_date: new Date().toISOString(),
  });

  res.status(200).json({
    message: 'Load created successfully',
  });
});

// @desc Get user's active load(if exists)
// @route GET /api/loads/active
// @access Private
const getActiveLoads = asyncHandler(async (req, res) => {
  const {id, role} = await User.findById(req.user.id);
  const load = await Load.findOne({assigned_to: id});

  checkRole(role, typeUser[1], res);

  if (!load || load.status === 'SHIPPED') {
    res.status(400);
    throw new Error(`Driver with id ${id} do not have active load`);
  }

  res.status(200).json({
    load,
  });
});

// @desc Iterate to next Load state
// @route PATCH /api/loads/active/state
// @access Private
const iterateToNextLoadState = asyncHandler(async (req, res) => {
  const {id, role} = await User.findById(req.user.id);
  const load = await Load.findOne({assigned_to: id});
  const truck = await Truck.findOne({assigned_to: id});

  checkRole(role, typeUser[1], res);

  if (!load || load.status === 'SHIPPED') {
    res.status(400);
    throw new Error(`Driver with id ${id} do not have active load`);
  }

  if (load.status === 'POSTED') {
    const newLogs = load.logs;

    newLogs.push({
      message: `Load state changed to ${statusLoadValue[2]}`,
      time: new Date().toISOString(),
    });

    await Load.findByIdAndUpdate(
        load.id,
        {
          status: statusLoadKey[2],
          state: statusLoadValue[2],
          logs: newLogs,
        },
        {new: true},
    );

    return res.status(200).json({
      message: `Load state changed to ${statusLoadValue[2]}`,
    });
  }

  if (load.status === 'ASSIGNED') {
    const newLogs = load.logs;
    newLogs.push({
      message: `Load state changed to ${statusLoadValue[3]}`,
      time: new Date().toISOString(),
    });
    await Load.findByIdAndUpdate(
        load.id,
        {
          status: statusLoadKey[3],
          state: statusLoadValue[3],
          logs: newLogs,
        },
        {new: true},
    );

    await Truck.findByIdAndUpdate(
        truck.id,
        {
          status: 'IS',
        },
        {new: true},
    );
    return res.status(200).json({
      message: `Load state changed to ${statusLoadValue[3]}`,
    });
  }
});

// @desc Get user's Load by id
// @route GET /api/loads/{id}
// @access Private
const getLoadById = asyncHandler(async (req, res) => {
  const load = await Load.findById(req.params.id);
  const user = await User.findById(req.user.id);

  // check for user
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (load.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  res.status(200).json({
    load,
  });
});

// @desc Update user's load by id
// @route PUT /api/loads/{id}
// @access Private
const updateLoadById = asyncHandler(async (req, res) => {
  const load = await Load.findById(req.params.id);
  const user = await User.findById(req.user.id);

  checkRole(user.role, typeUser[0], res);

  // check for user
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (load.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  if (load.status !== 'NEW') {
    res.status(400);
    throw new Error('Shipper is not able to update load when it not new');
  }

  await Load.findByIdAndUpdate(req.params.id, req.body, {new: true});

  res.status(200).json({
    message: 'Load details changed successfully',
  });
});

// @desc Delete user's load by id
// @route DELETE /api/loads/{id}
// @access Private
const deleteLoadById = asyncHandler(async (req, res) => {
  const load = await Load.findById(req.params.id);
  const user = await User.findById(req.user.id);

  checkRole(user.role, typeUser[0], res);

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  // check for user
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (load.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  if (load.status !== 'NEW') {
    res.status(400);
    throw new Error('Shipper is not able to update load when it not new');
  }

  await Load.deleteOne({_id: req.params.id});

  res.status(200).json({
    message: 'Load deleted successfully',
  });
});

// @desc Post a user's load by id
// @route POST /api/loads/{id}/post
// @access Private
const postLoadById = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user.id);
  const trucks = await Truck.find({});
  const assignedTruck = trucks.filter((truck) => truck.assigned_to !== null);

  checkRole(user.role, typeUser[0], res);

  const load = await Load.findById(req.params.id);

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (load.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  const {dimensions, payload} = load;

  if (load.status === statusLoadKey[1]) {
    res.status(400);
    throw new Error('Load is already posted');
  }

  const suitable = typeTruck.filter((truck) => {
    if (
      dimensions.width <= truck.width &&
      dimensions.length <= truck.length &&
      dimensions.height <= truck.height &&
      payload <= truck.payload
    ) {
      return truck;
    }
  });

  const suitableTruck = assignedTruck.find((truck) =>
    suitable.find(
        (el) => el.name === truck.type && truck.status === statusTruck[1],
    ),
  );

  if (!suitable || !suitableTruck || suitableTruck.status === statusTruck[0]) {
    res.status(400);
    throw new Error('We do not have suitable truck');
  }

  const newLogs = load.logs;
  newLogs.push({
    message: `Load assigned to driver with id ${suitableTruck.assigned_to}`,
    time: new Date().toISOString(),
  });

  await Load.findByIdAndUpdate(
      req.params.id,
      {
        assigned_to: suitableTruck.assigned_to,
        status: statusLoadKey[1],
        state: statusLoadValue[1],
        logs: newLogs,
      },
      {new: true},
  );

  await Truck.findByIdAndUpdate(
      suitableTruck.id,
      {
        status: statusTruck[0],
      },
      {new: true},
  );

  res.status(200).json({
    message: 'Load posted successfully',
    driver_found: true,
  });
});

// @desc Get user's Load shipping info by id
// @route GET /api/loads/{id}/shipping_info
// @access Private
const getLoadShippingById = asyncHandler(async (req, res) => {
  const load = await Load.findById(req.params.id);
  const trucks = await Truck.find();
  const activeTruck = trucks.find(
      (truck) => truck.assigned_to === load.assigned_to,
  );

  const user = await User.findById(req.user.id);

  checkRole(user.role, typeUser[0], res);

  // check for user
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (load.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  res.status(200).json({
    load,
    truck: activeTruck,
  });
});

module.exports = {
  getLoads,
  addLoad,
  getActiveLoads,
  iterateToNextLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingById,
};
