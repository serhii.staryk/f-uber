/* eslint-disable new-cap */
const express = require('express');

const {
  getTrucks,
  addTruck,
  getTruckByID,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('../controllers/truckControllers');

const protectRoute = require('../middlewares/authMiddleware');

const router = express.Router();

// Get user's trucks
router.get('/', protectRoute, getTrucks);
// Add Truck for User
router.post('/', protectRoute, addTruck);
// Get user's truck by id
router.get('/:id', protectRoute, getTruckByID);
// Update user's truck by id
router.put('/:id', protectRoute, updateTruckById);
// Delete user's truck by id
router.delete('/:id', protectRoute, deleteTruckById);
// Assign truck to user by id
router.post('/:id/assign', protectRoute, assignTruckById);

module.exports = router;
