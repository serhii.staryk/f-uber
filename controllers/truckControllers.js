/* eslint-disable max-len */
const asyncHandler = require('express-async-handler');
const Truck = require('../models/truckModel');
const User = require('../models/userModel');

const {typeTruck, statusTruck} = require('../models/types/truckType');

const typeUser = require('../models/types/userType');

const isExist = require('../utils/isExist');
const checkRole = require('../utils/checkRole');

const allowedTypeOfTrucks = typeTruck.map((item) => item.name);

// @desc Get user's trucks
// @route GET /api/trucks
// @access Private
const getTrucks = asyncHandler(async (req, res) => {
  const {id, role} = await User.findById(req.user.id);
  checkRole(role, typeUser[1], res);

  const trucks = await Truck.find({created_by: id});
  res.status(200).json({
    trucks,
  });
});

// @desc Add Truck for User
// @route POST /api/trucks
// @access Private
const addTruck = asyncHandler(async (req, res) => {
  const {id, role} = await User.findById(req.user.id);
  const type = req.body.type;

  checkRole(role, typeUser[1], res);

  if (!isExist(allowedTypeOfTrucks, type)) {
    res.status(400);
    throw new Error(
        `Available only next type: ${allowedTypeOfTrucks.join(', ')}`,
    );
  }

  await Truck.create({
    created_by: id,
    assigned_to: null,
    type,
    status: statusTruck[1],
    created_date: new Date().toISOString(),
  });

  res.status(200).json({
    message: 'Truck created successfully',
  });
});

// @desc Get user's truck by id
// @route GET /api/trucks/{id}
// @access Private
const getTruckByID = asyncHandler(async (req, res) => {
  const {role} = await User.findById(req.user.id);
  const truck = await Truck.findById(req.params.id);

  checkRole(role, typeUser[1], res);

  if (!truck) {
    res.status(400);
    throw new Error('Truck not found');
  }

  res.status(200).json({
    truck,
  });
});

// @desc Update user's truck by id
// @route PUT /api/trucks/{id}
// @access Private
const updateTruckById = asyncHandler(async (req, res) => {
  const newType = req.body.type;
  const truck = await Truck.findById(req.params.id);
  const user = await User.findById(req.user.id);

  checkRole(user.role, typeUser[1], res);

  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (!truck) {
    res.status(400);
    throw new Error('Truck not found');
  }

  if (truck.status !== statusTruck[1]) {
    res.status(400);
    throw new Error(
        'Driver is not able to change truck info while he is on a load',
    );
  }

  if (!isExist(allowedTypeOfTrucks, newType)) {
    res.status(400);
    throw new Error(
        `Available only next type: ${allowedTypeOfTrucks.join(', ')}`,
    );
  }

  await Truck.findByIdAndUpdate(
      req.params.id,
      {
        type: newType,
      },
      {new: true},
  );
  res.status(200).json({
    message: 'Truck details changed successfully',
  });
});

// @desc Delete user's truck by id
// @route DELETE /api/trucks/{id}
// @access Private
const deleteTruckById = asyncHandler(async (req, res) => {
  const truck = await Truck.findById(req.params.id);
  const {role} = await User.findById(req.user.id);

  checkRole(role, typeUser[1], res);

  if (!truck) {
    res.status(400);
    throw new Error('Truck not found');
  }

  if (truck.status !== statusTruck[1]) {
    res.status(400);
    throw new Error('Driver is not able to delete truck while he is on a load');
  }

  await Truck.deleteOne({_id: req.params.id});

  res.status(200).json({
    message: 'Truck deleted successfully',
  });
});

// @desc Assign truck to user by id
// @route POST /api/trucks/{id}/assign
// @access Private
const assignTruckById = asyncHandler(async (req, res) => {
  const truck = await Truck.findById(req.params.id);
  const trucks = await Truck.find();
  const {id, role} = await User.findById(req.user.id);

  checkRole(role, typeUser[1], res);

  if (!truck) {
    res.status(400);
    throw new Error('Truck not found');
  }

  const isAssigned = trucks.some((truck) => truck.assigned_to === id);

  if (truck.assigned_to !== null || isAssigned) {
    res.status(400);
    throw new Error(
        'Truck is already assigned or driver is already assigned to another Truck',
    );
  }

  await Truck.findByIdAndUpdate(
      req.params.id,
      {
        assigned_to: id,
      },
      {new: true},
  );

  res.status(200).json({
    message: 'Truck assigned successfully',
  });
});

module.exports = {
  getTrucks,
  addTruck,
  getTruckByID,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
