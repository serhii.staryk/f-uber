const jwt = require('jsonwebtoken');
const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');

const protectRoute = asyncHandler(async (req, res, next) => {
  let jwttoken;

  if (
    (req.headers.authorization &&
      req.headers.authorization.startsWith('Bearer')) ||
    (req.headers.authorization && req.headers.authorization.startsWith('JWT'))
  ) {
    try {
      // Get token from header
      jwttoken = req.headers.authorization.split(' ')[1];

      // Verify token
      const decoded = jwt.verify(jwttoken, process.env.JWT_SECRET);

      // Get user from token

      req.user = await User.findById(decoded.id).select('-password');
      next();
    } catch (error) {
      console.log(error);
      res.status(401);
      throw new Error('Not authorized');
    }
  }

  if (!jwttoken) {
    res.status(401);
    throw new Error('Not authorized, no token');
  }
});

module.exports = protectRoute;
